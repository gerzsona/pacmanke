﻿#include <stdlib.h> 
#include <iostream>
#include <conio.h>
#include <stdio.h>

using namespace std;

//map[i][j] in {0,      1,    2,     3,         4}
//              empty,  wall, token, player(1), player(2).
char answer, key;
bool help = false;
int gamemode, value;
bool instantdeath = false;
int map[30][25];
int mapmentes[30][25];
int rx, ry; // random generalt uj koordinata
char nextstep;
bool alive = true;
bool play = true;
int botOne[2] = {};
int botTwo[2];
int playerOne[3] = { 1, 1, 1 }; //x koord, y koord, HP,
int playerTwo[3] = { 28, 16, 1 }; // -"- 

#define KEY_W 119
#define KEY_A 97
#define KEY_S 115
#define KEY_D 100
#define KEY_I 105
#define KEY_J 106
#define KEY_K 107
#define KEY_L 108
#define KEY_N 110
#define KEY_Q 113
#define KEY_H 104
#define KEY_Y 121

void gamemodeset() {
    cout << "                               Valaszd ki a jatekmodot!" << endl;
    cout << "1 jatekos 2 ellenseg | 2 jatekos PvP | 2 jatekos PvP (gyors) | 2 jatekos 2 ellenseg" << endl;
    cout << "         (1)                (2)                (3)                      (4)" << endl;
    bool chosen = false;
    while (chosen == false) {
        cin >> gamemode;
        switch (gamemode) {
        case 1:
            cout << "Sajnos ez meg nincs kesz." << endl;
            break;
        case 2:
            chosen = true;
            break;
        case 3:
            instantdeath = true;
            chosen = true;
            break;
        case 4:
            cout << "Sajnos ez meg nincs kesz." << endl;
            break;
        default:
            cout << "Nincs ilyen jatekmod." << endl;
            break;
        }
    }
}


void botOneAI() {

}

void mapolvasas() {
    cout << "Kerlek masold ide a terkepet!" << endl;
    for (int i = 0; i < 25; ++i) {
        for (int j = 0; j < 30; ++j) {
            cin >> map[j][i];
        }
    }
    system("cls");
}

void draw() {
    system("cls");
    for (int i = 0; i < 25; ++i) {
        for (int j = 0; j < 30; ++j) {
            switch (map[j][24 - i]) {
            case 0: cout << "  ";
                break;
            case 1: cout << "||";
                break;
            case 2: cout << "$$";
                break;
            case 3: cout << "()";
                break;
            case 4: cout << "><";
                break;
            case 5: cout << playerOne[2] << "|";
                break;
            case 6: cout << "|" << playerTwo[2];
                break;
            case 7: cout << "[]";
                break;
            default: cout << "  ";
                break;
            }
        }
        cout << endl;
    }
}

void respawnOne() {
    bool correct = false;
    while (correct == false) {
        rx = rand() % 28 + 1;
        ry = rand() % 23 + 1;
        if (map[rx][ry] == 0) {
            playerOne[0] = rx;
            playerOne[1] = ry;
            map[rx][ry] = 3;
            correct = true;
        }
    }
}

void respawnTwo() {
    bool correct = false;
    while (correct == false) {
        rx = rand() % 28 + 1;
        ry = rand() % 23 + 1;
        if (map[rx][ry] == 0) {
            playerTwo[0] = rx;
            playerTwo[1] = ry;
            map[rx][ry] = 4;
            correct = true;
        }
    }
}

void move() {
    key = _getch();
    value = key;
    switch (value) {
    case KEY_D:
        switch (map[playerOne[0] + 1][playerOne[1]]) {
        case 0:
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[0] += 1;
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        case 2:
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[0] += 1;
            ++playerOne[2];
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        case 4:
            if (playerOne[2] > playerTwo[2]) {
                playerTwo[2] -= 1;
                if (instantdeath) {
                    playerOne[2] = 0;
                }
                map[playerOne[0]][playerOne[1]] = 0;
                playerOne[0] += 1;
                map[playerOne[0]][playerOne[1]] = 3;
                if (playerTwo[2] < 1) {
                    draw();
                    alive = false;
                }
                else {
                    respawnTwo();
                }
            }
            if (playerOne[2] < playerTwo[2]) {
                map[playerOne[0]][playerOne[1]] = 0;
                playerOne[2] -= 1;
                if (instantdeath) {
                    playerOne[2] = 0;
                }
                if (playerOne[2] < 1) {
                    draw();
                    alive = false;
                }
                else {
                    respawnOne();
                }
            }
            break;
        case 8:
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[0] += 1;
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        case 9:
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[0] += 1;
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        default:
            break;
        }
        break;
    case KEY_A:
        switch (map[playerOne[0] - 1][playerOne[1]]) {
        case 0:
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[0] -= 1;
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        case 2:
            ++playerOne[2];
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[0] -= 1;
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        case 4:
            if (playerOne[2] > playerTwo[2]) {
                playerTwo[2] -= 1;
                if (instantdeath) {
                    playerTwo[2] = 0;
                }
                map[playerOne[0]][playerOne[1]] = 0;
                playerOne[0] -= 1;
                map[playerOne[0]][playerOne[1]] = 3;
                if (playerTwo[2] < 1) {
                    draw();
                    alive = false;
                }
                else {
                    respawnTwo();
                }
            }
            if (playerOne[2] < playerTwo[2]) {
                map[playerOne[0]][playerOne[1]] = 0;
                playerOne[2] -= 1;
                if (instantdeath) {
                    playerOne[2] = 0;
                }
                if (playerOne[2] < 1) {
                    draw();
                    alive = false;
                }
                else {
                    respawnOne();
                }
            }
            break;
        case 8:
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[0] -= 1;
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        case 9:
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[0] -= 1;
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        default:
            break;
        }
    break;
    case KEY_W:
        switch (map[playerOne[0]][playerOne[1] + 1]) {
        case 0:
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[1] += 1;
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        case 2:
            ++playerOne[2];
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[1] += 1;
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        case 4:
            if (playerOne[2] > playerTwo[2]) {
                playerTwo[2] -= 1;
                if (instantdeath) {
                    playerTwo[2] = 0;
                }
                map[playerOne[0]][playerOne[1]] = 0;
                playerOne[1] += 1;
                map[playerOne[0]][playerOne[1]] = 3;
                if (playerTwo[2] < 1) {
                    draw();
                    alive = false;
                }
                else {
                    respawnTwo();
                }
            }
            if (playerOne[2] < playerTwo[2]) {
                map[playerOne[0]][playerOne[1]] = 0;
                playerOne[2] -= 1;
                if (instantdeath) {
                    playerOne[2] = 0;
                }
                if (playerOne[2] < 1) {
                    draw();
                    alive = false;
                }
                else {
                    respawnOne();
                }
            }
            break;
        case 7: /*
            if (map[16][1] == 4) {
                if (playerOne[2] > playerTwo[2]) {
                    playerTwo[2] -= 1;
                    if (instantdeath) {
                        playerTwo[2] = 0;
                    }
                    map[playerOne[0]][playerOne[1]] = 0;
                    playerOne[0] = 16;
                    playerOne[1] = 1;
                    map[playerOne[0]][playerOne[1]] = 3;
                    if (playerTwo[2] < 1) {
                        draw();
                        alive = false;
                    }
                    else {
                        respawnTwo();
                    }
                }
                if (playerOne[2] < playerTwo[2]) {
                    map[playerOne[0]][playerOne[1]] = 0;
                    playerOne[2] -= 1;
                    if (instantdeath) {
                        playerOne[2] = 0;
                    }
                    if (playerOne[2] < 1) {
                        draw();
                        alive = false;
                    }
                    else {
                        respawnOne();
                    }
                }
            }
            else {
                map[playerOne[0]][playerOne[1]] = 0;
                playerOne[0] = 16;
                playerOne[1] = 1;
                map[playerOne[0]][playerOne[1]] = 3;
            } */
            break;
        case 8:
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[1] += 1;
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        case 9:
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[1] += 1;
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        default:
            break;
        }
        break;
    case KEY_S:
        switch (map[playerOne[0]][playerOne[1] - 1]) {
        case 0:
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[1] -= 1;
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        case 2:
            ++playerOne[2];
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[1] -= 1;
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        case 4:
            if (playerOne[2] > playerTwo[2]) {
                playerTwo[2] -= 1;
                if (instantdeath) {
                    playerTwo[2] = 0;
                }
                map[playerOne[0]][playerOne[1]] = 0;
                playerOne[1] -= 1;
                map[playerOne[0]][playerOne[1]] = 3;
                if (playerTwo[2] < 1) {
                    draw();
                    alive = false;
                }
                else {
                    respawnTwo();
                }
            }
            if (playerOne[2] < playerTwo[2]) {
                map[playerOne[0]][playerOne[1]] = 0;
                playerOne[2] -= 1;
                if (instantdeath) {
                    playerOne[2] = 0;
                }
                if (playerOne[2] < 1) {
                    draw();
                    alive = false;
                }
                else {
                    respawnOne();
                }
            }
            break;
        case 8:
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[1] -= 1;
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        case 9:
            map[playerOne[0]][playerOne[1]] = 0;
            playerOne[1] -= 1;
            map[playerOne[0]][playerOne[1]] = 3;
            break;
        default:
            break;
        }
        break;
    case KEY_L:
        switch (map[playerTwo[0] + 1][playerTwo[1]]) {
        case 0:
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[0] += 1;
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        case 2:
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[0] += 1;
            ++playerTwo[2];
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        case 3:
            if (playerTwo[2] > playerOne[2]) {
                playerOne[2] -= 1;
                if (instantdeath) {
                    playerOne[2] = 0;
                }
                map[playerTwo[0]][playerTwo[1]] = 0;
                playerTwo[0] += 1;
                map[playerTwo[0]][playerTwo[1]] = 4;
                if (playerOne[2] < 1) {
                    draw();
                    alive = false;
                }
                else {
                    respawnOne();
                }
            }
            if (playerTwo[2] < playerOne[2]) {
                map[playerTwo[0]][playerTwo[1]] = 0;
                playerTwo[2] -= 1;
                if (instantdeath) {
                    playerTwo[2] = 0;
                }
                if (playerTwo[2] < 1) {
                    draw();
                    alive = false;
                }
                else {
                    respawnTwo();
                }
            }
            break;
        case 8:
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[0] += 1;
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        case 9:
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[0] += 1;
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        default:
            break;
        }
        break;
    case KEY_J:
        switch (map[playerTwo[0] - 1][playerTwo[1]]) {
        case 0:
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[0] -= 1;
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        case 2:
            ++playerTwo[2];
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[0] -= 1;
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        case 3:
            if (playerTwo[2] > playerOne[2]) {
                playerOne[2] -= 1;
                if (instantdeath) {
                    playerOne[2] = 0;
                }
                map[playerTwo[0]][playerTwo[1]] = 0;
                playerTwo[0] -= 1;
                map[playerTwo[0]][playerTwo[1]] = 4;
                if (playerOne[2] < 1) {
                    draw();
                    alive = false;
                }
                else {
                    respawnOne();
                }
            }
            if (playerTwo[2] < playerOne[2]) {
                map[playerTwo[0]][playerTwo[1]] = 0;
                playerTwo[2] -= 1;
                if (instantdeath) {
                    playerTwo[2] = 0;
                }
                if (playerTwo[2] < 1) {
                    draw();
                    alive = false;
                }
                else {
                    respawnTwo();
                }
            }
            break;
        case 8:
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[0] -= 1;
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        case 9:
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[0] -= 1;
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        default:
            break;
        }
        break;
    case KEY_I:
        switch (map[playerTwo[0]][playerTwo[1] + 1]) {
        case 0:
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[1] += 1;
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        case 2:
            ++playerTwo[2];
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[1] += 1;
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        case 3:
            if (playerTwo[2] > playerOne[2]) {
                playerOne[2] -= 1;
                if (instantdeath) {
                    playerOne[2] = 0;
                }
                map[playerTwo[0]][playerTwo[1]] = 0;
                playerTwo[1] += 1;
                map[playerTwo[0]][playerTwo[1]] = 4;
                if (playerOne[2] < 1) {
                    draw();
                    alive = false;
                }
                else {
                    respawnTwo();
                }
            }
            if (playerTwo[2] < playerOne[2]) {
                map[playerTwo[0]][playerTwo[1]] = 0;
                playerTwo[2] -= 1;
                if (instantdeath) {
                    playerTwo[2] = 0;
                }
                if (playerTwo[2] < 1) {
                    draw();
                    alive = false;
                }
                else {
                    respawnTwo();
                }
            }
            break;
        case 8:
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[1] += 1;
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        case 9:
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[1] += 1;
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        default:
            break;
        }
        break;
    case KEY_K:
        switch (map[playerTwo[0]][playerTwo[1] - 1]) {
        case 0:
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[1] -= 1;
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        case 2:
            ++playerTwo[2];
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[1] -= 1;
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        case 3:
            if (playerTwo[2] > playerOne[2]) {
                playerOne[2] -= 1;
                if (instantdeath) {
                    playerOne[2] = 0;
                }
                map[playerTwo[0]][playerTwo[1]] = 0;
                playerTwo[1] -= 1;
                map[playerTwo[0]][playerTwo[1]] = 4;
                if (playerOne[2] < 1) {
                    alive = false;
                    draw();
                }
                else {
                    respawnOne();
                }
            }
            if (playerTwo[2] < playerOne[2]) {
                map[playerTwo[0]][playerTwo[1]] = 0;
                playerTwo[2] -= 1;
                if (instantdeath) {
                    playerTwo[2] = 0;
                }
                if (playerTwo[2] < 1) {
                    alive = false;
                    draw();
                }
                else {
                    respawnTwo();
                }
            }
            break;
        case 8:
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[1] -= 1;
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        case 9:
            map[playerTwo[0]][playerTwo[1]] = 0;
            playerTwo[1] -= 1;
            map[playerTwo[0]][playerTwo[1]] = 4;
            break;
        default:
            break;
        }
        break;
    case KEY_Q: alive = false;
        break;
    case KEY_H:
        help = true;
        cout << endl << "'wasd'-vel az egyes jatekost." << endl << "A nyilakkal a kettes jatekost tudod iranyitani." << endl << "'X'-el ki tudsz lepni az aktualis jatekmenetbol" << endl << "h-val ujra tudod kerni a helplistet." << endl;
        break;
    default:
        cout << endl << "Nincs ilyen parancs! A parancslistaert irj be egy 'h' betut!" << endl;
        break;
    }
}

void reset() {
    playerOne[0] = 1;
    playerOne[1] = 1;
    playerOne[2] = 1;
    playerTwo[0] = 28;
    playerTwo[1] = 16;
    playerTwo[2] = 1;
    alive = true;
    for (int i = 0; i < 30; ++i) {
        for (int j = 0; j < 25; ++j) {
            map[i][j] = mapmentes[i][j];
        }
    }
}
int main() {
    mapolvasas();
    for (int i = 0; i < 30; ++i) {
        for (int j = 0; j < 25; ++j) {
            mapmentes[i][j] = map[i][j];
        }
    }

    while (play == true) {
        gamemodeset();
        reset();
        draw();
        while (alive == true) {
            move();
            draw();
        }
        if (playerOne[2] < 1) {
            cout << endl << "><" << " nyerte a jatekot!" << endl;
        }
        if (playerTwo[2] < 1) {
            cout << endl << "()" << " nyerte a jatekot!" << endl;
        }
        cout << endl << "Szeretnel meg egyet jatszani? Ha igen ird be ezt; 'Y'! Ha nem, akkor 'N'!" << endl;

        bool decided = false;

        while (decided == false) {
            key = _getch();
            value = key;
            switch (value) {
            case KEY_Y:
                play = true;
                decided = true;
                break;
            case KEY_N:
                cout << "Sajnalom! Legyen szep napotok!";
                play = false;
                decided = true;
                break;
            default: cout << "Ezt sajnos nem ertettem :(" << endl;
                break;
            }
        }
    }
}